package oocl.afs.todolist.service.mapper;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.dto.TodoUpdateRequest;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    private TodoMapper() {
    }
    public static Todo toEntity(TodoCreateRequest request) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(request, todo);
        return todo;
    }

    public static TodoResponse toResponse(Todo todoEntity) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todoEntity, todoResponse);
        return todoResponse;
    }
    public static Todo toEntityUpdate(TodoUpdateRequest request) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(request, todo);
        return todo;
    }
}
