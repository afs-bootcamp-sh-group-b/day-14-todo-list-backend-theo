package oocl.afs.todolist.service;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.dto.TodoUpdateRequest;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> findAll() {
        return todoRepository.findAll()
                .stream()
                .map(todoEtity -> TodoMapper.toResponse(todoEtity))
                .collect(Collectors.toList());
    }

    public TodoResponse createTodo(TodoCreateRequest todoCreateRequest){
        Todo newTodo = TodoMapper.toEntity(todoCreateRequest);
        newTodo.setDone(false);
        return TodoMapper.toResponse(todoRepository.save(newTodo));
    }

    public TodoResponse updateTodo(TodoUpdateRequest todoUpdateRequest){
        return TodoMapper.toResponse(todoRepository.save(TodoMapper.toEntityUpdate(todoUpdateRequest)));
    }

    public void deleteTodo(Long id){
        todoRepository.deleteById(id);
    }
}
