package oocl.afs.todolist;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TodoRepository todoRepository;
    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }
    @Test
    void should_find_todos() throws Exception {
        Todo todo = new Todo();
        todo.setText("Study React");
        todoRepository.save(todo);

        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo.getText()));
    }
    @Test
    void should_add_todo() throws Exception {
        String json = "{\"text\":\"Study React\"}";
        mockMvc.perform(post("/todo").content(json).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("text").value("Study React"));
    }

    @Test
    void should_update_todo_text() throws Exception {
        Todo todo = new Todo();
        todo.setText("Study React");
        Todo savedTodo=todoRepository.save(todo);
        Long id=savedTodo.getId();
        String json = "{\"id\":1,\"text\":\"first to do item\",\"done\":false}";
        mockMvc.perform(put("/todo/"+id).content(json).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("text").value("first to do item"));
    }

    @Test
    void should_update_todo_done() throws Exception {
        Todo todo = new Todo();
        todo.setText("Study React");
        Todo savedTodo=todoRepository.save(todo);
        Long id=savedTodo.getId();
        String json = "{\"id\":1,\"text\":\"Study React\",\"done\":true}";
        mockMvc.perform(put("/todo/"+id).content(json).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("done").value("true"));
    }
    @Test
    void should_delete_todo() throws Exception {
        Todo todo = new Todo();
        todo.setText("Study React");
        Todo savedTodo=todoRepository.save(todo);
        Long id=savedTodo.getId();
        mockMvc.perform(delete("/todo/"+id))
                .andExpect(MockMvcResultMatchers.status().isOk());
        boolean success=todoRepository.findById(id).isPresent();
        Assertions.assertFalse(success);

    }
}
